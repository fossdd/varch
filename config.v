module main

import json
import os
import net.http

struct Config {
pub mut:
	user_input   bool
	bootloader   string
	mountpoint   string
	keyboard     string
	timezone     string
	filesystem   string
	disk         string
	boot_disk    string
	root_disk    string
	hostname     string
	password     string
	all_packages []string
	add_packages []string
	run          []string
	post_run     []string
	tools        []string
}

// Ask for settings or get settings from config files; returns a config struct
fn get_config(args []string) ?Config {
	mut default_all_packages := ['base', 'linux', 'linux-firmware'] // Inital Arch Linux
	default_all_packages << ['amd-ucode', 'intel-ucode'] // Microcode
	default_all_packages << ['networkmanager', 'iwd'] // Network Manager
	default_all_packages << ['grub'] // Grub
	if os.is_dir('/sys/firmware/efi') {
		default_all_packages << 'efibootmgr'
	}

	mut config := Config{
		all_packages: default_all_packages
	}
	if args.len > 1 {
		path := get_local_path(args[1]) ?
		text := os.read_file(path) ?
		config = json.decode(Config, text) ?

		if config.all_packages == [] {
			config.all_packages = default_all_packages
		}

		if config.add_packages != [] {
			config.all_packages << config.add_packages
		}

		if config.tools != [] {
			tool_packages := get_tool_packages()
			for tool in config.tools {
				config.all_packages << tool_packages[tool]
			}
		}
	} else {
		config.user_input = true
	}

	if os.is_dir('/sys/firmware/efi') {
		config.bootloader = 'efi'
	} else {
		config.bootloader = 'bios'
	}

	config.keyboard = input(config, config.keyboard, 'Keyboard', 'us', false)
	config.timezone = input(config, config.timezone, 'Timezone', 'UTC', false)
	config.filesystem = input(config, config.filesystem, 'Filesystem', 'ext4', false)
	config.disk = input(config, config.disk, 'Disk', '/dev/sda', true)
	config.hostname = input(config, config.hostname, 'Hostname', 'localhost', true)
	config.password = input(config, config.password, 'Root Password', 'root', true)

	config.boot_disk = config.disk + '1'
	config.root_disk = config.disk + '2'
	config.mountpoint = '/mnt'

	return config
}

fn input(config Config, var string, text string, default string, force_enable bool) string {
	mut output := var
	if var == '' {
		mut ask := force_enable
		if config.user_input {
			ask = true
		}
		if ask {
			output = os.input('$text [$default]: ')
		}

		if output == '' {
			output = default
		}
	}
	return output
}

fn get_local_path(path string) ?string {
	if path.starts_with('http') {
		out := '/tmp/varch.json'
		http.download_file(path, out) ?
		return out
	} else {
		return path
	}
}
