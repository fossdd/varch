#!/usr/bin/env -S sudo v run

// Install archiso
system('pacman -S archiso --noconfirm')

// Setup profile
system('cp -r /usr/share/archiso/configs/releng iso')

// Add glibc
system('echo "glibc" >> iso/packages.x86_64')

// Set motd
system("echo 'To install Arch Linux run: `varch`\n' | tee iso/airootfs/etc/motd")

// Build varch
system('v .')
mv('varch', 'iso/airootfs/usr/local/bin/varch')?
chmod('iso/airootfs/usr/local/bin/varch', 755)

// Make ISO
system('mkarchiso -v -w work -o out iso')

// Clean
rmdir_all('iso') or {}
rmdir_all('work') or {}
