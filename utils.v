module main

import os

// Execute the array of commands
fn exec(command string) {
	println('Running `$command`')
	os.system(command)
}

// Execute the array of commands in a chroot
fn exec_chroot(command string, chroot_dir string) {
	println('Running `$command` in chroot')
	escaped_command := command.replace('"', '"')
	os.system('echo "$escaped_command" | arch-chroot $chroot_dir')
}
