module main

fn get_tool_packages() map[string][]string {
	mut packages := map[string][]string{}
	packages['gnome'] = ['gnome']
	packages['ssh'] = ['openssh']
	packages['libvirt'] = ['ebtables', 'dnsmasq', 'bridge-utils', 'virt-manager', 'virt-viewer']
	return packages
}

fn install_tools(config Config) {
	for tool in config.tools {
		install_tool(tool, config)
	}
}

fn install_tool(tool string, config Config) {
	match tool {
		'gnome' {
			exec_chroot('systemctl enable gdm', config.mountpoint)
		}
		'ssh' {
			exec_chroot('systemctl enable sshd')
		}
		'libvirt' {
			exec_chroot('systemctl enable libvirtd')

			exec_chroot('ip link add link `ip address | grep -v lo | cut -d ' ' -f2 | tr ':' '\n' | awk NF | head -n1` name macvtap0 type macvtap mode bridge')
			exec_chroot('ip link set macvtap0 up')
		}
		else {
			panic('Tool $tool not found!')
		}
	}
}
