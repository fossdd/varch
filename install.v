module main

// Installs the new system
fn install(config Config) {
	// Unmount for safety
	if config.bootloader == 'efi' {
		exec('umount "$config.boot_disk" 2> /dev/null || true')
	}
	exec('umount "$config.root_disk" 2> /dev/null || true')

	// Timezone
	exec('timedatectl set-ntp true')

	// Partitioning
	match config.bootloader {
		'efi' {
			exec(
				'( sleep 0.5; echo g; echo n; echo; echo; echo +512M; echo t; echo 1; echo n; echo; echo; echo; echo w ) | fdisk -w always -W always ' +
				config.disk)
		}
		'bios' {
			exec(
				'( sleep 0.5; echo g; echo n; echo; echo; echo +1M; echo t; echo 4; echo n; echo; echo; echo; echo w ) | fdisk -w always -W always ' +
				config.disk)
		}
		else {}
	}

	// Formatting partitions
	if config.bootloader == 'efi' {
		exec('mkfs.fat -F 32 ' + config.boot_disk)
	}
	exec('yes | mkfs.' + config.filesystem + ' ' + config.root_disk)

	// Mount our new partition
	exec('mount ' + config.root_disk + ' ' + config.mountpoint)

	// Install packages
	exec('pacstrap $config.mountpoint ' + config.all_packages.join(' '))

	// Setup fstab
	exec('genfstab -U $config.mountpoint >> $config.mountpoint/etc/fstab')

	for command in config.run {
		exec(command)
	}

	// Chroot commands

	// Time and date configuration
	exec_chroot('ln -sf /usr/share/zoneinfo/' + config.timezone + ' /etc/localtime', config.mountpoint)
	exec_chroot('hwclock --systohc', config.mountpoint)

	// Setup locales
	exec_chroot('sed -i "s/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/" /etc/locale.gen', config.mountpoint)
	exec_chroot('locale-gen', config.mountpoint)
	exec_chroot('echo "LANG=en_US.UTF-8" > /etc/locale.conf', config.mountpoint)

	// Setup hostname and hosts file
	exec_chroot('echo "$config.hostname" > /etc/hostname', config.mountpoint)
	exec_chroot('echo -e "127.0.0.1\tlocalhost" >> /etc/hosts', config.mountpoint)
	exec_chroot('echo -e "::1\t\tlocalhost" >> /etc/hosts', config.mountpoint)
	exec_chroot('echo -e "127.0.1.1\t$config.hostname" >> /etc/hosts', config.mountpoint)
	exec_chroot('chpasswd <<< "root:$config.password"', config.mountpoint)

	// Install GRUBv2 as a removable drive (universal across hw)
	match config.bootloader {
		'efi' {
			exec_chroot('mkdir /boot/efi', config.mountpoint)
			exec_chroot('mount "$config.boot_disk" /boot/efi', config.mountpoint)
			exec_chroot('grub-install --efi-directory=/boot/efi --bootloader-id=GRUB --removable',
				config.mountpoint)
			exec_chroot('grub-mkconfig -o /boot/grub/grub.cfg', config.mountpoint)
		}
		'bios' {
			exec_chroot('grub-install --removable "$config.disk"', config.mountpoint)
			exec_chroot('grub-mkconfig -o /boot/grub/grub.cfg', config.mountpoint)
		}
		else {}
	}

	// Setup Network Manager
	exec_chroot('systemctl enable NetworkManager', config.mountpoint)

	if config.tools != [] {
		install_tools(config)
	}

	for command in config.post_run {
		exec_chroot(command, config.mountpoint)
	}
}
