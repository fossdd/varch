module main

import os

fn main() {
	mut config := get_config(os.args) ?
	install(config)
}
